<?php
/**
 * Created by PhpStorm.
 * User: jadeonprog
 * Date: 7/30/2020
 * Time: 9:54 AM
 */

namespace App\Http\Controllers;

use App\Components\Core\ResponseHelpers;
use App\Components\User\Repositories\UserRepository;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    use ResponseHelpers;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }


    /**
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $validate = validator($request->all(),[
            'email' => 'required|email',
            'password' => 'required',
        ]);
        $credentials = $request->only(['email', 'password']);
        $user = $this->userRepository->findBy('email',$request->only('email'));

        if($validate->fails()){
            return $this->sendResponseBadRequest("The given data was invalid.",$validate->errors()->messages());
        }

//        if (!$user || ! $token = auth()->attempt($credentials)) {
        if (!$user || ! $token = auth()->attempt($credentials)) {
            $error = [
                'email' => ['These credentials do not match our records.']
            ];

            return $this->sendResponseBadRequest("The given data was invalid.",$error);
        }

        return $this->respondWithToken($token);
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        $data = auth()->logout();

        return $this->sendResponseOk($data,'Successfully logged out');
    }

    /**
     * @param $token
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'token' => $token,
            'token_type'   => 'bearer',
            'expires_at'   => date('Y-m-d H:i:s', strtotime('+1 hour')),
        ]);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: jadeonprog
 * Date: 10/6/2017
 * Time: 6:15 AM
 */

namespace App\Http\Controllers;

use App\Components\Core\Utilities\Helpers;
use App\Components\Core\ResponseHelpers;
use App\Components\User\Repositories\UserRepository;
use Illuminate\Http\Request;

class UserController extends Controller
{
    use ResponseHelpers;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = validator($request->all(),[
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed|min:8',
        ]);

        if($validate->fails()){

            return $this->sendResponseBadRequest("The given data was invalid.",$validate->errors()->messages());
        }

        /** @var User $user */
        $user = $this->userRepository->create($request->all());

        if(!$user){
            $error = [
                'email' => "Oppss, something wenet wrong upon creating account"
            ];

            return $this->sendResponseBadRequest("The given data was invalid.",$error);
        }

        return $this->sendResponseCreated($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = $this->userRepository->getUser($id);

        if(!$user){
            return $this->sendResponseNotFound();
        }

        return $this->sendResponseOk($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $payload = $request->all();
        $validate = validator($payload,[
            'name' => 'required',
            'email' => 'required|email',
        ]);

        if($validate->fails()){
            return $this->sendResponseBadRequest($validate->errors()->first());
        }

        if(!Helpers::hasValue($payload['password'])){
            unset($payload['password']);
        }

        $updated = $this->userRepository->update($id,$payload);

        if(!$updated){
            return $this->sendResponseBadRequest("Failed update");
        }

        return $this->sendResponseUpdated();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // do not delete self
        if($id == auth()->user()->id){
            return $this->sendResponseForbidden();
        }

        try {
            $this->userRepository->delete($id);
        } catch (\Exception $e) {
            return $this->sendResponseBadRequest("Failed to delete");
        }

        return $this->sendResponseDeleted();
    }
}
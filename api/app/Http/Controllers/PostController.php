<?php
/**
 * Created by PhpStorm.
 * User: jadeonprog
 * Date: 7/23/2020
 * Time: 2:23 PM
 */

namespace App\Http\Controllers;

use App\Components\Core\ResponseHelpers;
use App\Components\Post\Repositories\PostRepository;
use Illuminate\Http\Request;

class PostController extends Controller
{
    use ResponseHelpers;

    /**
     * @var PostRepository
     */
    private $postRepository;

    /**
     * PostController constructor.
     * @param PostRepository $postRepository
     */
    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function index(Request $request)
    {
        return $this->postRepository->getAll($request->all());
    }
    /**
     * Store a newly created Post in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validate = validator($input,[
            'title' => 'required',
            'content' => 'required',
            'image' => 'required',
        ]);
        //insert image upload logic here

        if($validate->fails()){

            return $this->sendResponseBadRequest("The given data was invalid.",$validate->errors()->messages());
        }

        $input['user_id'] = auth()->user()->id;
        $input['slug'] = str_replace(' ','-',$input['title']);

        /** @var Post $post */
        $post = $this->postRepository->create($input);

        return $this->sendResponseCreated($post);
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $slug)
    {
        $input = $request->all();
        $validate = validator($input,[
            'title' => 'required',
            'content' => 'required',
            'image' => 'required',
        ]);

        if($validate->fails()) {
            return $this->sendResponseBadRequest($validate->errors()->first());
        }

        $input['slug'] = str_replace(' ','-',$input['title']);

        $post = $this->postRepository->findBy('slug',$slug);

        $updated = $this->postRepository->update($post->id,$input);

        if(!$updated){
            return $this->sendResponseBadRequest("Failed update");
        }

        return $this->sendResponseUpdated();
    }

    /**
     * Remove the specified post from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->postRepository->delete($id);
        } catch (\Exception $e) {
            return $this->sendResponseBadRequest("Failed to delete");
        }

        return $this->sendResponseDeleted();
    }

    /**
     * Display the specified post.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(string $slug)
    {
        $post = $this->postRepository->findBy('slug',$slug);

        if(!$post){
            return $this->sendResponseNotFound();
        }

        return $this->sendResponseOk($post);
    }
}
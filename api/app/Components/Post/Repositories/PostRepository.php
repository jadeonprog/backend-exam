<?php

namespace App\Components\Post\Repositories;

use App\Components\Core\BaseRepository;
use App\Components\Post\Models\Post;

class PostRepository extends BaseRepository
{
    public function __construct(Post $model)
    {
        parent::__construct($model);
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Pagination\LengthAwarePaginator
     */
    public function getAll($param)
    {
        return $this->get($param);
    }

    /**
     * @param $id
     * @return PostRepository|\Illuminate\Database\Eloquent\Model|mixed
     */
    public function getPostById($id)
    {
        //get specific Post
        return $this->findBy('id',$id);
    }

}
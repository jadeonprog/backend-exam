<?php

namespace App\Components\Post\Models;

use App\Components\User\Models\User;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'id', 'created_id','slug','title','content','image','user_id'
    ];

    /**
     * get all comments under this certain post
     * @return data object
     */
    public function comments(){
        $this->hasMany(Comment::class,'post_id');
    }

    /**
     * get post owner/creator
     * @return user object
     */
    public function user(){
        $this->belongsTo(User::class);
    }

}
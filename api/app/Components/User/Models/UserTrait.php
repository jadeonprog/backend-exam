<?php
/**
 * Created by PhpStorm.
 * User: jadeonprog <jadeonprog>
 * Date: 5/27/2017
 * Time: 7:55 AM
 */

namespace App\Components\User\Models;
use Hash;


/**
 * Class UserTrait
 * @package App\SOLAR\User\Models
 */
trait UserTrait
{
    /**
     * hashes password
     *
     * @param $password
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }

    /**
     * get post related to login user
     */
    public function posts(){
        $this->hasMany(Post::class,'creator_id');
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: jadeonprog <jadeonprog>
 * Date: 10/6/2017
 * Time: 6:37 AM
 */

namespace App\Components\User\Repositories;

use App\Components\Core\BaseRepository;
use App\Components\User\Models\User;
use App\Components\Core\Utilities\Helpers;

class UserRepository extends BaseRepository
{
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    /**
     * @param int $id
     * @return PostRepository|bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Model[]|mixed|null
     */
    public function getUser(int $id)
    {
        $user = $this->model->find($id);

        if($user) {
            return $user;
        };

        return false;
    }

}